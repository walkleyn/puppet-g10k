## Release 0.2.0 - 2019-06-07
  - Removed anchor pattern.
  - Added parameter to manage required git package.

## Release 0.1.10 - 2018-12-20
  - Add support for setting postrun in config file
  - Add version to archive filename so new versions are downloaded
  - Set default version to 0.5.6

## Release 0.1.9 - 2018-10-12
  - Added ability to pass a web proxy for archive to use to access the internet (optional).
  - Added ability to pass `additional_settings` for a source.

## Release 0.1.8 - 2018-07-27
  - Added g10k project url to readme.

## Release 0.1.7 - 2018-07-27
  - Added use cache fallback parameter.
  - Updated readme.

## Release 0.1.6 - 2018-07-23
  - Updated reference documentation.

## Release 0.1.5 - 2018-07-23
  - Added max extract worker parameter.
  - Added is quiet parameter.

## Release 0.1.4 - 2018-07-19
  - Added max worker parameter.

## Release 0.1.3 - 2018-06-20
  - Updated metadata to include dependencies and missing information.

## Release 0.1.2 - 2018-06-20
  - Updated documentation.
  - Added REFERENCE document.

## Release 0.1.1 - 2018-06-15
## Release 0.1.0 - 2018-06-15
  - Initial Release
